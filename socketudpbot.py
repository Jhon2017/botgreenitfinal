from socket import socket, AF_INET, SOCK_DGRAM
from bot import chathandler
# Create a UDP socket
sock = socket(AF_INET, SOCK_DGRAM)
# Bind the socket to the port
server_address = ('localhost', 10000)
sock.bind(server_address)
while True:
    data, address = sock.recvfrom(512)
    if data:
        res = chathandler(data)
        sock.sendto(res.encode("utf-8"), address)
