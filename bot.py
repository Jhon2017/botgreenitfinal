from chatbot import Chat, reflections, multiFunctionCall
from json import loads
from re import sub
import urllib.request
from urllib.parse import quote


def geturl(query):
    url = "https://es.wikipedia.org/w/api.php?action=query&origin=%2A&generator=search&prop=extracts&gsrsearch="+quote(query)+"&gsrlimit=1&exintro=1&explaintext=1&exchars=350&exlimit=1&format=json"
    return url


def whoIs(query, sessionID="general"):
    query = sub('[!@#$?.]', '', query).strip()
    url = geturl(query)
    response = urllib.request.urlopen(url).read()
    resdict = loads(response.decode('unicode-escape'))
    resp = resdict.get("query").get("pages")
    key = [i for i in resp.keys()]
    if len(key) > 0:
        return resp.get(key[0]).get("extract")
    return query


call = multiFunctionCall({"whoIs": whoIs})
chat = Chat("green.template", reflections, call=call)


def initiateChat(senderID):
    chat._startNewSession(senderID)
    chat.conversation[senderID].append("Di Hola")


def respondToClient(senderID, message):
    chat.attr[senderID] = {"match": None, "pmatch": None, "_quote": False}
    chat.conversation[senderID].append(message)
    message = message.rstrip(".! \n\t")
    result = chat.respond(message, sessionID=senderID)
    chat.conversation[senderID].append(result)
    del chat.attr[senderID]
    return result


def chathandler(data):
    if isinstance(data, bytes):
        data = loads(data)
    senderID = data.get("uuid")
    if senderID not in chat.conversation:
        initiateChat(senderID)
    return respondToClient(senderID, data.get("message"))
